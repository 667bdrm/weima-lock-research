[Weima](http://www.r-wima.com) [A6121 electro mechanical cylinder](http://www.r-wima.com/en/Pro.asp?BigClassID=1)  is  smart cylinder with mechanical keys authentication. Each key has [ISO/IEC 7816](https://en.wikipedia.org/wiki/ISO/IEC_7816) capable authentication chip [Nationz](https://www.nationstech.com) [Z8IDA](https://www.nationstech.com/en/IDA/), which is queried by the lock controller on insert and unjamming the cylinder allowing to open the door by the key. If the lock controller does not recognize the key authentication chip or lock attempted opened by the lockpicks, the cylinder is blocked by the electrical solenoid inside, the cylinder controller reporting alert vie ZigBee interface. ZigBee protocol implemented by [Tuya TYZS5](https://developer.tuya.com/en/docs/iot/device-development/module/zigbee-module/tyzs5-zigbee-module-datasheet) module (TY-ZB-Board V1.2) which based on [EFR32](https://www.silabs.com/wireless/zigbee) SoC by [Silicon Labs](https://www.silabs.com)

Key contacts wiring:

![Key inside PCB wiring](https://gitlab.com/667bdrm/weima-lock-research/-/wikis/uploads/9bac12c1fa23e5d33602f3df0a7ec9f9/weima-key.jpg)

Cylinder pinout:

![Cylinder pinout](https://gitlab.com/667bdrm/weima-lock-research/-/wikis/uploads/f9b62ac540a10f1767193f2d62df7cf0/weima-cylinder-pinout.jpg)

1, 2, 3 are shorten to the GND on cylinder key move

[Nationz Z8IDA Datasheet](https://gitlab.com/667bdrm/weima-lock-research/-/wikis/uploads/8d0f538c8f125d4736f6006ad5ae41e4/1-1P32H23I3.pdf)

[User's Manual](https://gitlab.com/667bdrm/weima-lock-research/-/wikis/uploads/aca3159629ddc6b07489310be83ba91c/user-manual-4033606.pdf)